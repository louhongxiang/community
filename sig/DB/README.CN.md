# DB
[English](./README.md) | 简体中文

*DB SIG作为工作组，将整个数据库生态系统引入openEuler社区。*<br>
*\*注意\*:由于DB SIG意与全球贡献者一起工作，所以我们建议所有的PR开发流程或issue描述应该是英文版本。*<br>


## DB SIG 工作目标和范围
- 负责openEuler数据库开源软件包的维护，包括Mysql、MariaDB、PostgreSQL。
- 引入更多的OLTP数据库到openEuler，如openGauss或任何其他流行的开源数据库到openEuler社区。
- 围绕数据库引入生态系统工具，构建完整、丰富的数据库生产线软件生态系统。


## 成员

### Maintainer列表
- Zhenyu Zheng[@ZhengZhenyu](https://gitee.com/ZhengZhenyu), *zheng.zhenyu@outlook.com*
- Bo Zhao[@bzhaoop](https://gitee.com/bzhaoop), *bzhaojyathousandy@gmail.com*

### Committer列表
- Qide Chen[@dillon_chen](https://gitee.com/dillon_chen), *dillon.chen@turbolinux.com.cn*
- Zhenyu Zheng[@ZhengZhenyu](https://gitee.com/ZhengZhenyu), *zheng.zhenyu@outlook.com*
- Bo Zhao[@bzhaoop](https://gitee.com/bzhaoop), *bzhaojyathousandy@gmail.com*


## 组织会议
- TBD

### 联系方式
- [MailList](dev@openeuler.org) *dev@openeuler.org*
- [slack](https://join.slack.com/t/slack-jma9373/shared_invite/zt-o66x6a3a-HY4Cwjc49XPxc9aN_FHOdg)
- **微信** *如果您对本SIG组感兴趣，请将您的个人微信账号发给Bo Zhao, 他会将您拉入DB SIG微信群组。*

# 项目清单

*<项目名称和申请表格一致，具体地址可以在申请下来以后在刷新>*

项目名称 && repository地址：
- https://gitee.com/src-openeuler/bucardo
- https://gitee.com/src-openeuler/derby
- https://gitee.com/src-openeuler/firebird
- https://gitee.com/src-openeuler/foomatic
- https://gitee.com/src-openeuler/foomatic-db
- https://gitee.com/src-openeuler/geolatte-geom
- https://gitee.com/src-openeuler/glassfish-legal
- https://gitee.com/src-openeuler/gupnp-dlna
- https://gitee.com/src-openeuler/h2
- https://gitee.com/src-openeuler/mariadb-connector-odbc
- https://gitee.com/src-openeuler/mysql5
- https://gitee.com/src-openeuler/perl-DBIx-Safe
- https://gitee.com/src-openeuler/pgadmin4-server
- https://gitee.com/src-openeuler/pgpool2
- https://gitee.com/src-openeuler/postgresql
- https://gitee.com/src-openeuler/postgresql-odbc
- https://gitee.com/src-openeuler/unixODBC
- https://gitee.com/src-openeuler/mariadb
- https://gitee.com/src-openeuler/mariadb-connector-c
- https://gitee.com/src-openeuler/sqlite
- https://gitee.com/src-openeuler/perl-DBD-SQLite
- https://gitee.com/src-openeuler/perl-DBD-MySQL
